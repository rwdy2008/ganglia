#!/bin/sh
#used for transfer DHD Redis data ffrom prod backup to stage

ProdWeb=`/bin/lhshow d-p-w1 | awk '{print $2}'`

Global_slave=`ssh $ProdWeb "cat /var/www/dragonsmobile/config/game.yml | grep -A 4 "global_slave" | grep host | awk '{print \\$2}'"`

for i in 1 2 3 4 5 6 7 8 9 10
do
       Rm=`ssh $ProdWeb "cat /var/www/dragonsmobile/config/game.yml | grep -A 41 "data_slave_cluster" | egrep -A3 "${i}:" | grep "host" | \
awk '{print \\$2}'"`
        redis_host[$i]=$Rm
#        ((j++))
done

echo -en "\033[32mPlease input PROD realm,support much variable[eg:globle/job/user/realm_1]: \033[0m" 
read Realm

echo -en "\033[32mPlease input sudo passwd: \033[0m"
read -s Pass

echo

IP=`/bin/lhshow d-s-r | grep -v "*" | awk '{print $2}'`
Dir=$HOME

echo -e  "\033[32m      BackupName: \033[0m"

for realm in ${Realm}
do
	Num=`echo $realm | grep "_" | cut -d_ -f2`
        if [ $realm == "globle" ];then
                GlobleTime=`ssh ${Global_slave} "ls -lrt /var/local/backup/redis/*globle_8951* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                Globle="redis_cache_globle_8951.${GlobleTime}.tar.gz"
                GlobleFile="redis_cache_globle_8951"
                echo -e "\033[32m       $Globle\033[0m"
        elif [ $realm == "job" ];then
                JobTime=`ssh ${Global_slave} "ls -lrt /var/local/backup/redis/*globle_8953* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                Job="redis_cache_globle_8953.${JobTime}.tar.gz"
                JobFile="redis_cache_globle_8953"
                echo -e "\033[32m       $Job\033[0m"
        elif [ $realm == "user" ];then
                UserTime=`ssh ${Global_slave} "ls -lrt /var/local/backup/redis/*globle_8952* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                User="redis_cache_globle_8952.${UserTime}.tar.gz"
                UserFile="redis_cache_globle_8952"
                UserTime=`ssh ${Global_slave} "ls -lrt /var/local/backup/redis/*globle_8952* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                echo -e "\033[32m       $User\033[0m"
        elif [ $Num -eq 2 ];then
                REALMTime2=`ssh r3-6c-appb "ls -lrt /var/local/backup/redis/redis_cache_${realm}* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                REALM2="redis_cache_${realm}.${REALMTime2}.tar.gz"
                REALMFile2="redis_cache_${realm}"
                echo -e "\033[32m       $REALM2\033[0m"
	elif [ $Num -eq 4 ];then
                REALMTime4=`ssh r3-6c-appb "ls -lrt /var/local/backup/redis/redis_cache_${realm}* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                REALM4="redis_cache_${realm}.${REALMTime4}.tar.gz"
                REALMFile4="redis_cache_${realm}"
                echo -e "\033[32m       $REALM4\033[0m"
	elif [ $Num -eq 9 ];then
		REALMTime9=`ssh r4-8d-appb "ls -lrt /var/local/backup/redis/redis_cache_${realm}* | awk '{print $NF}' | tail -1 | cut -d. -f 2"`
                REALM9="redis_cache_${realm}.${REALMTime9}.tar.gz"
                REALMFile9="redis_cache_${realm}"
                echo -e "\033[32m       $REALM9\033[0m"
        fi

done

echo -en "\033[31mAre you ture[Y/N]: \033[0m"
read ANS

if [ $ANS == "Y" ] || [ $ANS == "y" ];then
	:
else
	echo -e "\033[31mInput error\033[0m"
	exit 0
fi

for realm in ${Realm}
do
	RN=`echo $realm| cut -d_ -f2`
        if [ $realm == "globle" ];then
                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${Global_slave}:/var/local/backup/redis/$Globle $Dir"
                echo -e "\033[32mUnpacking... \033[0m"
                ssh $IP "tar zxf $Dir/$Globle && echo $Pass | sudo -S rm -rf /redis/redis_cache_globle/dump_8133.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_globle_8951/dump_8951.rdb /redis/redis_cache_globle/dump_8133.rdb && echo $Pass | sudo -S /etc/init.d/redis_8133 restart"
        elif [ $realm == "job" ];then
                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${Global_slave}:/var/local/backup/redis/$Job $Dir"
		echo -e "\033[32mUnpacking... \033[0m"
		ssh $IP "tar zxf $Dir/$Job && echo $Pass | sudo -S rm -rf /redis/redis_cache_realm_5/dump_8138.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_globle_8953/dump_8953.rdb /redis/redis_cache_realm_5/dump_8138.rdb && echo $Pass | sudo -S /etc/init.d/redis_8138 restart"
        elif [ $realm == "user" ];then
                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${Global_slave}:/var/local/backup/redis/$User $Dir"
		echo -e "\033[32mUnpacking... \033[0m"
                ssh $IP "tar zxf $Dir/$User && echo $Pass | sudo -S rm -rf /redis/redis_cache_realm_4/dump_8137.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_globle_8952/dump_8952.rdb /redis/redis_cache_realm_4/dump_8137.rdb && echo $Pass | sudo -S /etc/init.d/redis_8137 restart"
        elif [ $RN -eq 2 ];then    
                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${redis_host[$RN]}:/var/local/backup/redis/$REALM2 $Dir"
		echo -e "\033[32mUnpacking... \033[0m"
		ssh $IP "tar zxf $Dir/$REALM2 && echo $Pass | sudo -S rm -rf /redis/redis_cache_realm_1/dump_8134.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_realm_2/dump_8135.rdb /redis/redis_cache_realm_1/dump_8134.rdb && echo $Pass | sudo -S /etc/init.d/redis_8134 restart"
	elif [ $RN -eq 4 ];then
		ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${redis_host[$RN]}:/var/local/backup/redis/$REALM4 $Dir"
                echo -e "\033[32mUnpacking... \033[0m"
		ssh $IP "tar zxf $Dir/$REALM4 && echo $Pass | sudo -S rm -rf /redis/redis_cache_realm_2/dump_8135.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_realm_4/dump_8137.rdb /redis/redis_cache_realm_2/dump_8135.rdb && echo $Pass | sudo -S /etc/init.d/redis_8135 restart"
	elif [ $RN -eq 9 ];then
		ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${redis_host[$RN]}:/var/local/backup/redis/$REALM9 $Dir"
		echo -e "\033[32mUnpacking... \033[0m"
		ssh $IP "tar zxf $Dir/$REALM9 && echo $Pass | sudo -S rm -rf /redis/redis_cache_realm_3/dump_8136.rdb && echo $Pass | sudo -S cp $Dir/redis/redis_cache_realm_9/dump_8142.rdb /redis/redis_cache_realm_3/dump_8136.rdb && echo $Pass | sudo -S /etc/init.d/redis_8136 restart"
        fi
done
