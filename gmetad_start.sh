#!/bin/sh
# $Id: gmetad.init 2181 2010-01-07 15:56:11Z d_pocock $
#
# chkconfig: - 20 80
# description: gmetad startup script
#
GMETAD=/usr/sbin/gmetad

. /etc/rc.d/init.d/functions

test -f /etc/sysconfig/gmetad && . /etc/sysconfig/gmetad

export RRDCACHED_ADDRESS

RETVAL=0

case "$1" in
   start)
      ISMOUNTED=`mount | grep -c /var/lib/ganglia`
      if [ $ISMOUNTED -lt 1 ] ; then
        /bin/mount -t tmpfs tmpfs /var/lib/ganglia
        cp -pR /var/lib/ganglia-backup/* /var/lib/ganglia/
      fi

      echo -n "Starting GANGLIA gmetad: "
      [ -f $GMETAD ] || exit 1

      daemon $GMETAD
      RETVAL=$?
      echo
      [ $RETVAL -eq 0 ] && touch /var/lock/subsys/gmetad
      ;;

  stop)    
      echo -n "Shutting down GANGLIA gmetad: "
      killproc gmetad
      RETVAL=$?
      echo
      [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/gmetad

      ISMOUNTED=`mount | grep -c /var/lib/ganglia`
      if [ $ISMOUNTED -ge 1 ] ; then
        /usr/bin/rsync --archive --delete /var/lib/ganglia/* /var/lib/ganglia-persist/
        umount /var/lib/ganglia
      fi
      ;;

  restart|reload)
      $0 stop
      $0 start
      RETVAL=$?
      ;;
  status)
      status gmetad
      RETVAL=$?
      ;;
  *)
      echo "Usage: $0 {start|stop|restart|status}"
      exit 1
esac

exit $RETVAL
