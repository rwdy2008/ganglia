#!/bin/sh
#used for importing DHD DB data from prod backup to stage
echo -en "\033[32mPlease input PROD realm,support much variable[eg:cross/backup/realm1]: \033[0m" 
read Realm

echo -en "\033[32mPlease input time[eg: 201411200010]: \033[0m"
read Time

echo -en "\033[32mPlease input sudo passwd: \033[0m"
read -s Pass

echo

IP=`/bin/lhshow d-s-dm1 | grep -v "*" | awk '{print $2}'`

ProdWeb=`/bin/lhshow d-p-w1 | awk '{print $2}'`
Cross_host=`ssh $ProdWeb "cat /var/www/dragonsmobile/config/database.yml | grep -A 42 "slave"  | grep -A2 cross_realm  | grep host | awk '{print \\$2}'"`
Real_host=`ssh $ProdWeb "cat /var/www/dragonsmobile/config/database.yml | grep -A 42 "slave"  | grep -A2 realm_1:  | grep host | awk '{print \\$2}'"`

Dir=$HOME

echo -e  "\033[32m      BackupName: \033[0m"
for realm in $Realm
do	
	echo -e "\033[32m	doam_$realm.${Time}.dmp.gz"
done

echo -en "\033[31mAre you ture[Y/N]: \033[0m"
read ANS

if [ $ANS == "Y" ] || [ $ANS == "y" ];then
        :
else                                                                                         
        echo -en "\033[31mInput error!!! \033[0m"                                            
        exit 0                                                                               
fi                                                                                           
                                                                                             
for realm in ${Realm}                                                                        
do                                                                                           
        Data="doam_${realm}.${Time}.dmp.gz"                                                  
        File="doam_${realm}.${Time}.dmp"                                                     
        Num=`echo ${realm##*[a-zA-Z]}`
###transfer data                         
        if [[ $realm == "cross" || $realm == "backup" ]];then

                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${Cross_host}:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_'"$realm"'@doam_stage_'"$realm"'@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt"

        elif [[ $Num -ge 1 && $Num -le 10 ]] || [[ $Num -ge 31 && $Num -le 40 ]] || [[ $Num -ge 61 && $Num -le 70 ]] || [[ $Num -ge 91 && $Num -le 100 ]];then

                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@${Real_host}:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_realm@doam_stage_realm@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt" 

#        elif [[ $Num -ge 11 && $Num -le 20 ]] || [[ $Num -ge 41 && $Num -le 50 ]] || [[ $Num -ge 71 && $Num -le 80 ]] || [[ $Num -ge 101 && $Num -le 110 ]];then
#
#                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@r10-16b-dba:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_realm@doam_stage_realm@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt"
#
#        elif [[ $Num -ge 21 && $Num -le 30 ]] || [[ $Num -ge 51 && $Num -le 60 ]] || [[ $Num -ge 81 && $Num -le 90 ]] || [[ $Num -ge 111 && $Num -le 120 ]];then
#
#                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@r9-6b-dba:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_realm@doam_stage_realm@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt" 
#
#        elif [[ $Num -ge 121 && $Num -le 160 ]];then
#                                                                                             
#                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@r15-8c-dba:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_realm@doam_stage_realm@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt"
#
#        elif [[ $Num -ge 161 && $Num -le 196 ]];then
#                                                                                             
#                ssh -t $IP "echo $Pass | sudo -S scp -i /home/dragonsmobile/.ssh/id_rsa dragonsmobile@r20-10b-dba:/var/local/backup/mysql/$Data $Dir && md5sum $Dir/$Data && gunzip $Dir/$Data && sed -i 's@doam_realm@doam_stage_realm@g' $Dir/$File && echo $Pass | sudo -S mysql -e 'source $Dir/$File' > $File.txt"

	fi
done

